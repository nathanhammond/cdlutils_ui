var webpack = require('webpack');

var year = new Date().getFullYear();
var banner = 'CdlUtils-UI | Copyright (C) ' + year + ' Wenlin Institute, Inc. SPC. All Rights Reserved.\n'
	+ '\tThis JavaScript file is offered for licensing under the GNU Affero General Public License v.3:\n'
	+ '\t\thttps://www.gnu.org/licenses/agpl.html\n'
	+ '\tUse of the Wenlin CDL JavaScript API is subject to the Terms of Service:\n'
	+ '\t\thttps://wenlincdl.com/terms';

module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    webpack: {
      options: {
        plugins: [
          new webpack.BannerPlugin(banner)
        ],
        module: {
          loaders: [
            {
              test: /\.js$/,
              loader: 'eslint-loader',
            }
          ]
        },
        eslint: {  
          configFile: '.eslintrc'
        }
      },
      dist: {
        entry: './src/CdlUtilsUi.js',
        output: {
          filename: 'dist/cdl-utils-ui.js'
        }
      }
    },

    uglify: {
      options: {
        // uncomment the below to debug the minified JS
        // beautify: true,
        banner: '/*! ' + banner + ' */\n'
      },
      dist: {
        files: {
          'dist/cdl-utils-ui.min.js': ['dist/cdl-utils-ui.js']
        }
      }
    },

    clean: {
      pre: ['dist'],
      post: [
        'dist/*',
        '!dist/cdl-utils-ui.js',
        '!dist/cdl-utils-ui.min.js'
      ]
    }
  });

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-webpack');

  grunt.registerTask('default', ['clean:pre', 'webpack:dist', 'uglify:dist', 'clean:post']);
};