### Info ###

This repository contains a portion of the Wenlin CDL JavaScript API.

This project is for building cdl-utils-ui.js, which, along with cdl-utils.js, is available at http://wenlincdl.com/docs/getting-started

There are also NPM packages. See: https://www.npmjs.com/package/cdl-utils and https://www.npmjs.com/package/cdl-utils-ui

Files in this repository are offered for licensing under the AGPL v.3 license. See: https://www.gnu.org/licenses/agpl.html

For other licensing options, see: http://wenlin.com/developers

Much of the Wenlin CDL JavaScript API is derived from the C source code for the desktop (Mac/Windows/Linux) application Wenlin Software for Learning Chinese.
Authors: Tom Bishop, Richard Cook, David Chanin and other members of the Wenlin development team.

Info about the Wenlin CDL JavaScript API: see https://wenlincdl.com

Terms of Service: see https://wenlincdl.com/terms

Copyright (c) 2018 Wenlin Institute, Inc. SPC.

### building the browser JS file ###

CdlUtilsUi uses webpack to bundle all the JS together for the browser, and makes it available under the global `window.CdlUtils` object. To build:

`yarn install`

`grunt`

you may need to run `npm install -g grunt` if the `grunt` command isn't found

The compiled JS will be put into the `dist` folder.
