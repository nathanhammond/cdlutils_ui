module.exports = function(elm, styles) {
  for (var styleName in styles) {
    if (Object.prototype.hasOwnProperty.call(styles, styleName)) {
      elm.style[styleName] = styles[styleName];
    }
  }
};
