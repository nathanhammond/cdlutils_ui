var CharacterAnimator = require('./CharacterAnimator');
var StrokeMatcher = require('./StrokeMatcher');
var UserStrokeRenderer = require('./UserStrokeRenderer');
var callIfExists = require('./callIfExists');
var invoke = require('./invoke');
var extend = require('./extend');

var CharacterQuizzer = function(targetElm, options) {
  CharacterAnimator.call(this, targetElm, options);
  this._setupListeners();
};

// convenience method for calling superclass methods
var superclass = function() {
  return Object.getPrototypeOf(CharacterQuizzer.prototype);
};

// extends animator
CharacterQuizzer.prototype = Object.create(CharacterAnimator.prototype);
CharacterQuizzer.prototype.constructor = CharacterQuizzer;

CharacterQuizzer.prototype._setupListeners = function() {
  /* The 'svg' node may contain a 'g' node. If so, make sure to add the event listeners
     to the 'svg' node, which is the parent of the 'g' node.
     Typically, this.svg.nodeName = 'g' and this.svg.parentNode.nodeName = 'svg'. */
  var svgNode = this.svg.nodeName.toUpperCase() === 'G' ? this.svg.parentNode : this.svg;
  svgNode.addEventListener('mousedown', function(evt) {
    if (!this._isActive) return;
    evt.preventDefault();
    this._startUserStroke(this._getMousePoint(evt));
  }.bind(this));
  svgNode.addEventListener('touchstart', function(evt) {
    if (!this._isActive) return;
    evt.preventDefault();
    this._startUserStroke(this._getTouchPoint(evt));
  }.bind(this));
  svgNode.addEventListener('mousemove', function(evt) {
    if (!this._isActive) return;
    evt.preventDefault();
    this._continueUserStroke(this._getMousePoint(evt));
  }.bind(this));
  svgNode.addEventListener('touchmove', function(evt) {
    if (!this._isActive) return;
    evt.preventDefault();
    this._continueUserStroke(this._getTouchPoint(evt));
  }.bind(this));

  // TODO: fix
  global.document.addEventListener('mouseup', this._endUserStroke.bind(this));
  global.document.addEventListener('touchend', this._endUserStroke.bind(this));
};

CharacterQuizzer.prototype._getMousePoint = function(evt) {
  var box = this.svg.getBoundingClientRect();
  return {
    x: evt.clientX - box.left,
    y: evt.clientY - box.top
  };
};

CharacterQuizzer.prototype._getTouchPoint = function(evt) {
  var box = this.svg.getBoundingClientRect();
  return {
    x: evt.touches[0].clientX - box.left,
    y: evt.touches[0].clientY - box.top
  };
};

CharacterQuizzer.prototype.render = function(options) {
  var baseOptions = extend(this.options, options);
  this._strokeMatcher = new StrokeMatcher(baseOptions.width, baseOptions.height);
  return superclass().render.call(this, options).then(function() {
    this.highlightStrokes = invoke(this.strokes, 'clone');
    var highlightStrokeStyles = extend({ fill: '#AAF' }, this.options.highlightStrokeStyles);
    invoke(this.highlightStrokes, 'hide');
    invoke(this.highlightStrokes, 'setStyles', highlightStrokeStyles);
  }.bind(this));
};

CharacterQuizzer.prototype.startQuiz = function(options) {
  this.options = extend(this.options, options);
  this.resetQuiz();
  this._isActive = true;
};

CharacterQuizzer.prototype.resetQuiz = function() {
  this._currentStrokeIndex = 0;
  this._numRecentMistakes = 0;
  this._totalMistakes = 0;

  if (this._userStrokeRenderer) {
    this._userStrokeRenderer.remove();
  }
  invoke(this.strokes, 'hide');
  this._userStrokeRenderer = null;
  this._userStrokePoints = null;
};

CharacterQuizzer.prototype.animate = function(options) {
  invoke(this.highlightStrokes, 'hide');
  return superclass().animate(options);
};

CharacterQuizzer.prototype.reset = function() {
  superclass().reset.call(this);
  this.resetQuiz();
  invoke(this.highlightStrokes || [], 'remove');
  this.highlightStrokes = [];
};

CharacterQuizzer.prototype._startUserStroke = function(point) {
  if (!this._isActive) return null;
  if (this._userStrokePoints) return this._endUserStroke();

  // Reverse svg scale to capture correct coordinates
  this._reverseSvgScale(point);

  this._userStrokePoints = [point];
  this._userStrokeRenderer = new UserStrokeRenderer(this.svg, this._userStrokePoints, this.options);
  return null;
};

CharacterQuizzer.prototype._continueUserStroke = function(point) {
  if (!this._userStrokePoints) return;

  // Reverse svg scale to capture correct coordinates
  this._reverseSvgScale(point);

  this._userStrokePoints.push(point);
  this._userStrokeRenderer.updatePath();
};

CharacterQuizzer.prototype._reverseSvgScale = function(point) {
  svgScaleAni = this.svg.transform.animVal;
  svgScale = 1;
  // check matrix defined and not null, to avoid error on Safari
  if (svgScaleAni.length > 0 && svgScaleAni[0].matrix) {
    svgScale = svgScaleAni[0].matrix.a;
  }
  // console.log(svgScale);
  point.x = point.x / svgScale - this.options.x; // options.x is for margin
  point.y = point.y / svgScale - this.options.y; // options.y is for margin
};

CharacterQuizzer.prototype._getStrokesPoints = function() {
  return this.strokes.map(function(stroke) {
    return this._extractPointsFromStrokeAnimator(stroke);
  }.bind(this));
};

CharacterQuizzer.prototype._endUserStroke = function() {
  if (!this._userStrokePoints) return Promise.resolve();
  var promises = [];
  var matchingStrokeIndex = this._strokeMatcher.getMatchingStrokeIndex(this._userStrokePoints, this._getStrokesPoints());

  promises.push(this._userStrokeRenderer.fadeAndRemove());
  this._userStrokePoints = null;
  this._userStrokeRenderer = null;
  if (!this._isActive) return Promise.resolve();

  if (this._isValidStroke(matchingStrokeIndex)) {
    this._handleSuccess(matchingStrokeIndex);
  } else {
    this._handleFaiulure();
    if (this.options.showHintAfterMisses !== false && this._numRecentMistakes >= (this.options.showHintAfterMisses || 3)) {
      promises.push(this._highlightCorrectStroke());
    }
  }
  return Promise.all(promises);
};

CharacterQuizzer.prototype._handleSuccess = function(strokeIndex) {
  callIfExists(this.options.onCorrectStroke, {
    strokeNum: this._currentStrokeIndex,
    mistakesOnStroke: this._numRecentMistakes,
    totalMistakes: this._totalMistakes,
    strokesRemaining: this.strokes.length - this._currentStrokeIndex - 1
  });
  this._currentStrokeIndex += 1;
  this._numRecentMistakes = 0;
  var promise = this._fadeInStroke(strokeIndex);
  if (this._currentStrokeIndex === this.strokes.length) {
    callIfExists(this.options.onComplete, {
      totalMistakes: this._totalMistakes
    });
    if (this.options.highlightOnComplete) {
      var duration = this.options.highlightOnCompleteDuration || 1000;
      promise = promise.then(this._highlightCharacter.bind(this, duration));
    }
  }
  return promise;
};

CharacterQuizzer.prototype._fadeInStroke = function(strokeIndex) {
  var stroke = this.strokes[strokeIndex];
  var duration = this.options.strokeFadeInDuration || 300;
  stroke.setStyles({ opacity: 0 });
  stroke.show();
  return stroke.tweenStyles({ opacity: 1 }, { duration: duration });
};

CharacterQuizzer.prototype._highlightCharacter = function(duration) {
  return Promise.all(this.highlightStrokes.map(function(highlightStroke) {
    highlightStroke.setStyles({ opacity: 0 });
    highlightStroke.show();
    return highlightStroke.tweenStyles({ opacity: 1 }, { duration: duration / 2 }).then(function() {
      return highlightStroke.tweenStyles({ opacity: 0 }, { duration: duration / 2 });
    });
  }));
};

CharacterQuizzer.prototype._handleFaiulure = function() {
  this._numRecentMistakes += 1;
  this._totalMistakes += 1;
  callIfExists(this.options.onMissedStroke, {
    strokeNum: this._currentStrokeIndex,
    mistakesOnStroke: this._numRecentMistakes,
    totalMistakes: this._totalMistakes,
    strokesRemaining: this.strokes.length - this._currentStrokeIndex
  });
};

CharacterQuizzer.prototype._highlightCorrectStroke = function() {
  return this.highlightStrokes[this._currentStrokeIndex].animate({
    strokeAnimationVelocity: this.options.highlightStrokeVelocity || 1.5,
    animateStyles: {
      start: {
        opacity: 1
      },
      end: {
        opacity: 0
      }
    }
  });
};

CharacterQuizzer.prototype._isValidStroke = function(strokeIndex) {
  return strokeIndex === this._currentStrokeIndex;
};

CharacterQuizzer.prototype.stopQuiz = function() {
  this._isActive = false;
};

var POINTS_PER_STROKE = 20; // estimate a stroke using this many points. More is probably better but slower.
CharacterQuizzer.prototype._extractPointsFromStrokeAnimator = function(strokeAnimator) {
  var maskPath = strokeAnimator.maskPath;
  var length = maskPath.getTotalLength();
  var points = [];
  for (var i = 0; i < POINTS_PER_STROKE; i++) {
    points.push(maskPath.getPointAtLength(i * length / (POINTS_PER_STROKE - 1)));
  }
  return points;
};

// --------- Static convenience methods --------

// convenience method around new CharacterQuizzer, renderCharacter, startQuiz
CharacterQuizzer.setupQuiz = function(targetElm, charOrUnicode, options) {
  var quizzer = new CharacterQuizzer(targetElm, options);
  quizzer.renderCharacter(charOrUnicode).then(function() {
    quizzer.startQuiz();
  });
  return quizzer;
};

module.exports = CharacterQuizzer;
