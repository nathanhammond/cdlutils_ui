module.exports = function(elm, attrName, attrValue) {
  elm.setAttributeNS(null, attrName, attrValue);
};
