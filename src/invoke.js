module.exports = function(arr, methodName) {
  var args = Array.prototype.slice.call(arguments, 2);
  return arr.map(function(elm) {
    return elm[methodName].apply(elm, args);
  });
};
