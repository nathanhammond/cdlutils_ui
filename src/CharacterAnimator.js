var CdlUtils = global.CdlUtils;
var extend = require('./extend');
var invoke = require('./invoke');
var StrokeAnimator = require('./StrokeAnimator');
var StrokeOutlineRenderer = require('./StrokeOutlineRenderer');
var CharacterAnimationManager = require('./CharacterAnimationManager');
var setSvgAttr = require('./setSvgAttr');

var CharacterAnimator = function(targetElm, options) {
  this.options = options || {};
  this.options.x = this.options.x || 0;
  this.options.y = this.options.y || 0;
  this.options.width = this.options.width || 400;
  this.options.height = this.options.height || 400;

  if (typeof targetElm === 'string') {
    this.targetElm = document.getElementById(targetElm);
  } else {
    this.targetElm = targetElm;
  }
  if (!targetElm) {
    throw new Error('Invalid element passed in to CharacterAnimator');
  }
  var targetNodeType = this.targetElm.nodeName.toUpperCase();
  if (targetNodeType === 'SVG' || targetNodeType === 'G') {
    this.svg = this.targetElm;
  } else {
    this.svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    this.targetElm.appendChild(this.svg);
  }
  this.reset();
};

CharacterAnimator.prototype.renderCharacter = function(charOrUnicode, options) {
  this.reset();
  var opts = extend(this.options, options);
  return this.loadCharacter(charOrUnicode, opts).then(function() {
    return this.render(opts);
  }.bind(this));
};

CharacterAnimator.prototype.loadCharacter = function(charOrUnicode, options) {
  return CdlUtils.fetchCdlChar(charOrUnicode, extend(this.options, options)).then(function(cdlChar) {
    this.cdlChar = cdlChar;
    return this;
  }.bind(this));
};

CharacterAnimator.prototype.render = function(options) {
  this.reset();
  if (!this.cdlChar) return;
  this.defs = document.createElementNS('http://www.w3.org/2000/svg', 'defs');
  this.svg.appendChild(this.defs);
  var baseOptions = extend(this.options, options);
  setSvgAttr(this.svg, 'width', baseOptions.width + baseOptions.x);
  setSvgAttr(this.svg, 'height', baseOptions.height + baseOptions.y);
  // same options, but also need to get the plain font pathstrings.
  // we use the plain stroke pathstring as a mask while animating
  var plainFontOptions = extend(baseOptions, {font: 'plain'});
  this._ensureCdlChar();
  return Promise.all([
    CdlUtils.getSvgPathStrings(this.cdlChar, baseOptions),
    CdlUtils.getSvgPathStrings(this.cdlChar, plainFontOptions)
  ]).then(function(mirroredPathStrings) {
    var pathStrings = mirroredPathStrings[0]; // these are the actual pathstrings we need to draw
    var maskPathStrings = mirroredPathStrings[1]; // these are the plain font versions of those pathstrings used as a mask
    // draw all outlines first so they're behind all the strokes
    pathStrings.forEach(function(pathString) {
      this.strokeOutlines.push(new StrokeOutlineRenderer(this.svg, pathString, baseOptions));
    }.bind(this));
    for (var i = 0; i < pathStrings.length; i++) {
      this.strokes.push(new StrokeAnimator(this.svg, this.defs, pathStrings[i], maskPathStrings[i], baseOptions));
    }
  }.bind(this));
};

CharacterAnimator.prototype._ensureCdlChar = function() {
  if (!this.cdlChar) {
    throw new Error('You need to load a character before calling methods on a CharacterAnimator');
  }
};

CharacterAnimator.prototype.animate = function(animationOptions) {
  this.stopAnimation();

  var options = extend(this.options, animationOptions);
  this.animationManager = new CharacterAnimationManager(this.strokes, options);
  this.animationManager.run();
};

CharacterAnimator.prototype.stopAnimation = function() {
  if (this.animationManager) {
    this.animationManager.stop();
    this.animationManager = null;
  }
};

CharacterAnimator.prototype.updateOptions = function(options) {
  this.options = extend(this.options, options);
  invoke(this.strokes, 'updateOptions', options);
  invoke(this.strokeOutlines, 'updateOptions', options);
  if (this.animationManager) {
    this.animationManager.updateOptions(options);
  }
};

CharacterAnimator.prototype.reset = function() {
  this.stopAnimation();
  if (this.strokes) {
    invoke(this.strokes, 'remove');
    invoke(this.strokeOutlines, 'remove');
  }
  if (this.defs) {
    this.defs.remove();
  }
  this.strokes = [];
  this.strokeOutlines = [];
  this.isDrawn = false;
};

CharacterAnimator.prototype.setStrokeStyles = function(attrs) {
  invoke(this.strokes, 'setStyles', attrs);
};

CharacterAnimator.prototype.tweenStrokeStyles = function(attrs, options) {
  invoke(this.strokes, 'tweenStyles', attrs, options);
};

CharacterAnimator.prototype.setOutlineStyles = function(attrs) {
  invoke(this.strokeOutlines, 'setStyles', attrs);
};

CharacterAnimator.prototype.tweenOutlineStyles = function(attrs, options) {
  invoke(this.strokeOutlines, 'tweenStyles', attrs, options);
};

CharacterAnimator.prototype.hide = function() {
  invoke(this.strokes, 'hide');
};

CharacterAnimator.prototype.show = function() {
  return Promise.all(invoke(this.strokes, 'show'));
};

CharacterAnimator.prototype.hideOutline = function() {
  invoke(this.strokeOutlines, 'hide');
};

CharacterAnimator.prototype.showOutline = function() {
  return Promise.all(invoke(this.strokeOutlines, 'show'));
};

CharacterAnimator.prototype.remove = function() {
  this.stopAnimation();
  invoke(this.strokes, 'remove');
  invoke(this.strokeOutlines, 'remove');
  this.defs.remove();
  // only delete the svg node if we created it
  if (this.targetElm !== this.svg) {
    this.svg.remove();
  }
};

// --------- Static convenience methods --------

CharacterAnimator.animateCharacter = function(targetElm, charOrUnicode, options) {
  var animator = new CharacterAnimator(targetElm, options);
  animator.renderCharacter(charOrUnicode).then(function() {
    animator.animate();
  });
  return animator;
};

module.exports = CharacterAnimator;
