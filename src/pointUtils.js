var arrayMin = require('./arrayMin');
var arrayMax = require('./arrayMax');
var average = require('./average');

var pointUtils = {};

// return a new point subtracting point from this
pointUtils.subtract = function(pointA, pointB) {
  return { x: pointA.x - pointB.x, y: pointA.y - pointB.y };
};

pointUtils.getMagnitude = function(point) {
  return Math.sqrt(Math.pow(point.x, 2) + Math.pow(point.y, 2));
};

pointUtils.equals = function(pointA, pointB) {
  if (!pointA || !pointB) return false;
  return pointB.x === pointA.x && pointB.y === pointA.y;
};

pointUtils.getBounds = function(points) {
  var xs = points.map(function(point) { return point.x; });
  var ys = points.map(function(point) { return point.y; });
  var maxX = arrayMax(xs);
  var maxY = arrayMax(ys);
  var minX = arrayMin(xs);
  var minY = arrayMin(ys);
  return [{x: minX, y: minY}, {x: maxX, y: maxY}];
};

pointUtils.getDistance = function(point1, point2) {
  var difference = pointUtils.subtract(point1, point2);
  return pointUtils.getMagnitude(difference);
};

pointUtils.cosineSimilarity = function(point1, point2) {
  var rawDotProduct = point1.x * point2.x + point1.y * point2.y;
  return rawDotProduct / pointUtils.getMagnitude(point1) / pointUtils.getMagnitude(point2);
};

// get for each point in pointsA, find the min dist to a point in pointsB, then average
// TODO: consider also running the reverse calculation and averaging
pointUtils.getAvgMinDist = function(pointsA, pointsB) {
  var dists = pointsA.map(function(pointA) {
    var distsToB = pointsB.map(function(pointB) {
      return pointUtils.getDistance(pointA, pointB);
    });
    return arrayMin(distsToB);
  });
  return average(dists);
};

module.exports = pointUtils;
