var velocity = require('../vendor/velocity.min');
var extend = require('./extend');
var setSvgAttr = require('./setSvgAttr');
var setSvgStyles = require('./setSvgStyles');


var maskIdCounter = 0;

var StrokeAnimator = function(svg, defs, pathString, maskPathString, options) {
  this.svg = svg;
  this.defs = defs;
  this.pathString = pathString;
  this.maskPathString = maskPathString;
  this.options = options;

  this._render();
};

StrokeAnimator.prototype._render = function() {
  this.maskPath = document.createElementNS('http://www.w3.org/2000/svg', 'path');
  // mask needs to be wide enough to cover the whole stroke, but not too wide or weird stuff might happen
  // tweak this value to find something that works well
  var maskWidth = (this.options.width + this.options.height) / 10;
  // experiment with this to make sure it fully covers all strokes still
  var extensionAmount = (this.options.width + this.options.height) / 20;
  var maskId = 'stroke-mask-' + maskIdCounter;
  maskIdCounter += 1;
  setSvgAttr(this.maskPath, 'd', this.maskPathString);
  setSvgAttr(this.maskPath, 'fill', 'none');
  setSvgAttr(this.maskPath, 'stroke', '#FFF');
  setSvgAttr(this.maskPath, 'stroke-width', maskWidth);
  extendPath(this.maskPath, extensionAmount);
  // determine length after extending the path
  this.strokeLength = this.maskPath.getTotalLength();
  setSvgAttr(this.maskPath, 'stroke-dasharray', '' + this.strokeLength + ',' + this.strokeLength);
  // animating the stroke is accomplished by animating the stroke-dashoffset
  setSvgStyles(this.maskPath, { 'stroke-dashoffset': this.strokeLength });

  this.path = document.createElementNS('http://www.w3.org/2000/svg', 'path');
  // there's a bug in some browsers rendering vertical paths with masks. This is a trick to fix that bug.
  // http://stackoverflow.com/questions/19708943/svg-straight-path-with-clip-path-not-visible-in-chrome
  var modifiedPathString = 'M -10,-10 ' + this.pathString;
  setSvgAttr(this.path, 'd', modifiedPathString);
  setSvgAttr(this.path, 'mask', 'url(#' + maskId + ')');
  var defaultStrokeStyles = this.options.font === 'plain' ? { fill: 'none', stroke: '#000' } : { fill: '#000' };
  setSvgStyles(this.path, extend(defaultStrokeStyles, this.options.strokeStyles));

  this.mask = document.createElementNS('http://www.w3.org/2000/svg', 'mask');
  setSvgAttr(this.mask, 'id', maskId);

  this.defs.appendChild(this.mask);
  this.mask.appendChild(this.maskPath);
  this.svg.appendChild(this.path);
};

StrokeAnimator.prototype.updateOptions = function(options) {
  this.options = extend(this.options, options);
};

StrokeAnimator.prototype.show = function() {
  setSvgStyles(this.maskPath, { 'stroke-dashoffset': 0 });
};

StrokeAnimator.prototype.hide = function() {
  this.stopStrokeAnimation();
  setSvgStyles(this.maskPath, { 'stroke-dashoffset': this.maskPath.getTotalLength() });
};

StrokeAnimator.prototype.animate = function(animationOptions) {
  var options = extend(this.options, animationOptions);
  var strokeVelocity = options.strokeAnimationVelocity || 1;
  var duration = 2000 * this.strokeLength / (this.options.width + this.options.height) / strokeVelocity;
  this.hide();
  var animationPromises = [];
  if (options.animateStyles && options.animateStyles.start) {
    setSvgStyles(this.path, options.animateStyles.start);
  }
  animationPromises.push(velocity(this.maskPath, { 'stroke-dashoffset': 0 }, { duration: duration }));
  if (options.animateStyles && options.animateStyles.end) {
    animationPromises.push(velocity(this.path, options.animateStyles.end, { duration: duration }));
  }
  return Promise.all(animationPromises);
};

StrokeAnimator.prototype.setStyles = function(attrs) {
  setSvgStyles(this.path, attrs);
};

StrokeAnimator.prototype.tweenStyles = function(attrs, options) {
  return velocity(this.path, attrs, { duration: (options && options.duration) || 1000});
};

StrokeAnimator.prototype.stopStrokeAnimation = function() {
  velocity(this.maskPath, 'stop', true);
};

StrokeAnimator.prototype.stop = function() {
  velocity(this.path, 'stop', true);
  velocity(this.maskPath, 'stop', true);
};

StrokeAnimator.prototype.remove = function() {
  this.maskPath.remove();
  this.mask.remove();
  this.path.remove();
};

StrokeAnimator.prototype.clone = function() {
  return new StrokeAnimator(this.svg, this.defs, this.pathString, this.maskPathString, this.options);
};

// extend a non-closed SVG path
var extendPath = function(path, extensionAmount) {
  var pathLength = path.getTotalLength();
  var delta = pathLength / 100; // approximate a tangent line by looking at points spaced very close together

  var newStartPoint = extrapolatePoint(path.getPointAtLength(delta), path.getPointAtLength(0), extensionAmount);
  var newEndPoint = extrapolatePoint(path.getPointAtLength(pathLength - delta), path.getPointAtLength(pathLength), extensionAmount);
  var pathString = path.getAttribute('d');
  var newPathStart = 'M' + newStartPoint.x + ' ' + newStartPoint.y + ' L';
  var newPathEnd = ' L' + newEndPoint.x +  ' ' + newEndPoint.y;
  var newPathString = pathString.replace(/^M/, newPathStart) + newPathEnd;
  setSvgAttr(path, 'd', newPathString);
};

// given 2 points find another point <distance> pixels away on the same line
var extrapolatePoint = function(pointA, pointB, distance) {
  if (pointA.x === pointB.x) {
    var sign = pointB.y > pointA.y ? 1 : -1;
    return {
      x: pointA.x,
      y: pointB.y + sign * distance
    };
  }
  var slope = (pointB.y - pointA.y) / (pointB.x - pointA.x);
  var intercept = pointA.y - pointA.x * slope;
  var distBA = Math.sqrt(Math.pow(pointB.y - pointA.y, 2) + Math.pow(pointB.x - pointA.x, 2));
  var xDistBA = pointB.x - pointA.x;
  var xDist = xDistBA * distance / distBA;

  var x = pointB.x + xDist;
  var y = slope * x + intercept;
  return {x: x, y: y};
};

module.exports = StrokeAnimator;
