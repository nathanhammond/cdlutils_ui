var velocity = require('../vendor/velocity.min');
var extend = require('./extend');
var setSvgAttr = require('./setSvgAttr');
var setSvgStyles = require('./setSvgStyles');

var UserStrokeRenderer = function(svg, points, options) {
  this.svg = svg;
  this.points = points;
  this.options = options;

  this._render();
};

UserStrokeRenderer.prototype._render = function() {
  this.path = document.createElementNS('http://www.w3.org/2000/svg', 'path');
  this.updatePath();
  var styles = extend({
    fill: 'none',
    stroke: '#555',
    'stroke-width': 2,
    opacity: 1
  }, this.options.userStrokeStyles);
  setSvgStyles(this.path, styles);
  this.svg.appendChild(this.path);
};

UserStrokeRenderer.prototype.updateOptions = function(options) {
  this.options = extend(this.options, options);
};

UserStrokeRenderer.prototype.updatePath = function() {
  setSvgAttr(this.path, 'd', this.getPathString());
};

UserStrokeRenderer.prototype.setStyles = function(attrs) {
  setSvgStyles(this.path, attrs);
};

UserStrokeRenderer.prototype.tweenStyles = function(attrs, options) {
  return velocity(this.path, attrs, { duration: (options && options.duration) || 1000 });
};

UserStrokeRenderer.prototype.fadeAndRemove = function(options) {
  var opts = extend(this.options, options);
  var duration = opts.userStrokeFadeDuration || 500;
  return this.tweenStyles({ opacity: 0 }, { duration: duration }).then(function() {
    this.remove();
  }.bind(this));
};

UserStrokeRenderer.prototype.remove = function() {
  this.path.remove();
};

UserStrokeRenderer.prototype.getPathString = function() {
  var start = this.points[0];
  var remainingPoints = this.points.slice(1);
  var pathString = 'M ' + start.x + ' ' + start.y;
  remainingPoints.forEach(function(point) {
    pathString += ' L ' + point.x + ' ' + point.y;
  });
  return pathString;
};

module.exports = UserStrokeRenderer;
