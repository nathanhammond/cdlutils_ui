var average = require('./average');
var pointUtils = require('./pointUtils');

var AVG_DIST_THRESHOLD = 0.2; // 0 to 1, bigger = more lenient
var LENGTH_RATIO_THRESHOLD = 0.7; // 0 to 1, bigger = more lenient
var COSINE_SIMILARITY_THRESHOLD = 0;  // -1 to 1, smaller = more lenient
var START_AND_END_DIST_THRESHOLD = 0.3; // 0 to 1, bigger = more lenient

var StrokeMatcher = function(width, height) {
  this.size = (width + height) / 2;
};

StrokeMatcher.prototype.getMatchingStrokeIndex = function(userStrokePoints, strokesPoints) {
  var points = this._stripDuplicates(userStrokePoints);
  if (points.length < 2) return null;

  var closestStrokePoints = null;
  var closestStrokeIndex = 0;
  var bestAvgDist = 0;
  strokesPoints.forEach(function(strokePoints, i) {
    var avgDist = pointUtils.getAvgMinDist(points, strokePoints);
    if (avgDist < bestAvgDist || !closestStrokePoints) {
      closestStrokePoints = strokePoints;
      closestStrokeIndex = i;
      bestAvgDist = avgDist;
    }
  });

  var withinDistThresh = bestAvgDist < (this.size * AVG_DIST_THRESHOLD);
  var lengthAdjustFactor = (this.size / 10); // make ratio more fair for tiny strokes
  var lengthRatio = (this._getLength(points) + lengthAdjustFactor) / (this._getLength(closestStrokePoints) + lengthAdjustFactor);
  var withinLengthThresh = (lengthRatio > (1 - LENGTH_RATIO_THRESHOLD)) && (lengthRatio < (1 + LENGTH_RATIO_THRESHOLD));
  var startAndEndMatch = this._startAndEndMatches(points, closestStrokePoints);
  var directionMatches = this._directionMatches(points, closestStrokePoints);
  if (withinDistThresh && withinLengthThresh && startAndEndMatch && directionMatches) {
    return closestStrokeIndex;
  }
  return null;
};

StrokeMatcher.prototype._startAndEndMatches = function(points, closestStrokePoints) {
  var startingDist = pointUtils.getDistance(closestStrokePoints[0], points[0]);
  var endingDist = pointUtils.getDistance(closestStrokePoints[closestStrokePoints.length - 1], points[points.length - 1]);
  return startingDist < (this.size * START_AND_END_DIST_THRESHOLD) && endingDist < (this.size * START_AND_END_DIST_THRESHOLD);
};

StrokeMatcher.prototype._directionMatches = function(points, strokePoints) {
  var edgeVectors = this._getEdgeVectors(points);
  var strokeVectors = this._getEdgeVectors(strokePoints);
  var similarities = [];
  for (var i = 0; i < edgeVectors.length; i++) {
    var edgeVector = edgeVectors[i];
    var strokeSimilarities = strokeVectors.map(function(strokeVector) {
      return pointUtils.cosineSimilarity(strokeVector, edgeVector);
    });
    var maxSimilarity = Math.max.apply(Math, strokeSimilarities);
    similarities.push(maxSimilarity);
  }
  var avgSimilarity = average(similarities);
  return avgSimilarity > COSINE_SIMILARITY_THRESHOLD;
};

StrokeMatcher.prototype._stripDuplicates = function(points) {
  if (points.length < 2) return points;
  var dedupedPoints = [points[0]];
  points.slice(1).forEach(function(point) {
    if (!pointUtils.equals(point, dedupedPoints[dedupedPoints.length - 1])) {
      dedupedPoints.push(point);
    }
  });
  return dedupedPoints;
};

StrokeMatcher.prototype._getLength = function(points) {
  var length = 0;
  var lastPoint = points[0];
  for (var i = 0; i < points.length; i++) {
    var point = points[i];
    length += pointUtils.getDistance(point, lastPoint);
    lastPoint = point;
  }
  return length;
};

// returns a list of the direction of all segments in the line connecting the points
StrokeMatcher.prototype._getEdgeVectors = function(points) {
  var vectors = [];
  var lastPoint = points[0];
  points.slice(1).forEach(function(point) {
    vectors.push(pointUtils.subtract(point, lastPoint));
    lastPoint = point;
  });
  return vectors;
};

module.exports = StrokeMatcher;
