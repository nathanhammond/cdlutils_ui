var velocity = require('../vendor/velocity.min');
var extend = require('./extend');
var setSvgAttr = require('./setSvgAttr');
var setSvgStyles = require('./setSvgStyles');

var StrokeOutlineRenderer = function(svg, pathString, options) {
  this.svg = svg;
  this.pathString = pathString;
  this.options = options;

  this._render();
};

StrokeOutlineRenderer.prototype._render = function() {
  this.path = document.createElementNS('http://www.w3.org/2000/svg', 'path');
  setSvgAttr(this.path, 'd', this.pathString);
  var defaultStyles = this.options.font === 'plain' ? { stroke: '#DDD', fill: 'none' } : { fill: '#DDD' };
  var styles = extend(defaultStyles, this.options.strokeOutlineStyles);
  setSvgStyles(this.path, styles);
  if (this.options.showOutline) {
    this.show();
  } else {
    this.hide();
  }
  this.svg.appendChild(this.path);
};

StrokeOutlineRenderer.prototype.updateOptions = function(options) {
  this.options = extend(this.options, options);
};

StrokeOutlineRenderer.prototype.show = function() {
  this.setStyles({ 'visibility': 'visible' });
};

StrokeOutlineRenderer.prototype.hide = function() {
  this.setStyles({ 'visibility': 'hidden' });
};

StrokeOutlineRenderer.prototype.setStyles = function(attrs) {
  setSvgStyles(this.path, attrs);
};

StrokeOutlineRenderer.prototype.tweenStyles = function(attrs, options) {
  return velocity(this.path, attrs, { duration: (options && options.duration) || 1000 });
};

StrokeOutlineRenderer.prototype.remove = function() {
  this.path.remove();
};

StrokeOutlineRenderer.prototype.clone = function() {
  return new StrokeOutlineRenderer(this.svg, this.pathString, this.options);
};

module.exports = StrokeOutlineRenderer;
