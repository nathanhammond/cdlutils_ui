var velocity = require('../vendor/velocity.min');
var CharacterAnimator = require('./CharacterAnimator');
var CharacterQuizzer = require('./CharacterQuizzer');
var StrokeAnimator = require('./StrokeAnimator');
var CdlUtils = global.CdlUtils;

CdlUtils.velocity = velocity;
CdlUtils.Animator = CharacterAnimator;
CdlUtils.Quizzer = CharacterQuizzer;
CdlUtils.StrokeAnimator = StrokeAnimator;
